class Labirynth {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.maze=[];
    }
    generate() {
        var maze = [];
        var mazeWidth = this.x;
        var mazeHeight = this.y;
        var moves = [];
        for (var i = 0; i < mazeHeight; i++) {
            maze[i] = [];
            for (var j = 0; j < mazeWidth; j++) {
                maze[i][j] = 1;
            }
        }
        var posX = 1;
        var posY = 1;
        maze[posX][posY] = 0;
        moves.push(posY + posY * mazeWidth);


        while(moves.length!=0){
                var possibleDirections = "";
                if(posX+2 > 0 && posX + 2 < mazeHeight - 1 && maze[posX + 2][posY] == 1){
                     possibleDirections += "d";
                }
                if(posX-2 > 0 && posX - 2 < mazeHeight - 1 && maze[posX - 2][posY] == 1){
                     possibleDirections += "g";
                }
                if(posY-2 > 0 && posY - 2 < mazeWidth - 1 && maze[posX][posY - 2] == 1){
                     possibleDirections += "r";
                }
                if(posY+2 > 0 && posY + 2 < mazeWidth - 1 && maze[posX][posY + 2] == 1){
                     possibleDirections += "l";
                }
                if(possibleDirections){
                     var move = getRandomInt(0, possibleDirections.length - 1);
                     switch (possibleDirections[move]){
                          case "g":
                               maze[posX - 2][posY] = 0;
                               maze[posX - 1][posY] = 0;
                               posX -= 2;
                               break;
                          case "d":
                               maze[posX + 2][posY] = 0;
                               maze[posX + 1][posY] = 0;
                               posX += 2;
                               break;
                          case "r":
                               maze[posX][posY - 2] = 0;
                               maze[posX][posY - 1] = 0;
                               posY -= 2;
                               break;
                          case "l":
                               maze[posX][posY + 2]=0;
                               maze[posX][posY + 1]=0;
                               posY += 2;
                               break;
                     }
                     moves.push(posY + posX * mazeWidth);
                }
                else{
                     var back = moves.pop();
                     posX = Math.floor(back / mazeWidth);
                     posY = back % mazeWidth;
                }

        }
        this.maze=maze
       console.log(this.maze)
        this.draw()
        return this.maze
    }
    draw() {
        for(var i=0;i<this.maze.length;i++){
            for(var j=0;j<this.maze[i].length;j++){
             if(this.maze[i][j]==1){
                //objects.push(new Wall(new Vertex(i * dy, j * dy, -dy),new Vertex(0,0,0),dy,BrickMaterial))
                objects.push(new Wall(new Vertex(i * dy, j * dy, 0),new Vertex(0,0,0),dy,BrickMaterial))
             }else{
                // objects.push(new Plane(new Vertex(i * dy, j * dy, 0.5 * dy),new Vertex(0,0,0),dy,plane_material))
                // objects.push(new Plane(new Vertex(i * dy, j * dy, -0.5 * dy),new Vertex(0,Math.PI,0),dy,plane_material))
             }
            }
        }
        //objects.concat(tobjects)
        console.log(objects.length)


    }
}
