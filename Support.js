class Support {
    constructor() {

    }
    center_of_square(p1, p2) {
        return new Vertex((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2)
    }

    distance_p_to_p(p1, p2) {
        return Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2) + Math.pow(p1.z-p2.z, 2))
    }

    project(M) {
        // Distance between the camera and the plane
        // var a =0.66*Math.cos(playerrotation.z)
        // var b =0.66*Math.sin(playerrotation.z)
        // return new Vertex(M.x+a*M.y,M.z+b*M.y)

        var d = 700;
        var r = d / M.y;

        return new Vertex2D(r * M.x, r * M.z);
    }

    splitTriangle(triangle) {
        var p1 = triangle[1]
        var p2 = triangle[2]
        var p3 = triangle[0]
        var p6 = new Vertex((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2)
        var p4 = new Vertex((p1.x + p3.x) / 2, (p3.y + p1.y) / 2, (p3.z + p1.z) / 2)
        var p5 = new Vertex((p3.x + p2.x) / 2, (p3.y + p2.y) / 2, (p3.z + p2.z) / 2)
        return [
            [p1, p4, p6],
            [p4, p5, p6],
            [p2, p5, p6],
            [p3, p4, p5]
        ]
    }
    distance_2d(p1, p2) {
        return Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2))
    }
    macierz_obrotu(rotation) {
        var x = new Matrix([
            [1, 0, 0],
            [0, Math.cos(rotation.x), -Math.sin(rotation.x)],
            [0, Math.sin(rotation.x), Math.cos(rotation.x)]
        ])
        var y = new Matrix([
            [Math.cos(rotation.y), 0, Math.sin(rotation.y)],
            [0, 1, 0],
            [-Math.sin(rotation.y), 0, Math.cos(rotation.y)]
        ])
        var z = new Matrix([
            [Math.cos(rotation.z), -Math.sin(rotation.z), 0],
            [Math.sin(rotation.z), Math.cos(rotation.z), 0],
            [0, 0, 1]
        ])

        x.multiply(z)
        x.multiply(y)
        return x
    }

}